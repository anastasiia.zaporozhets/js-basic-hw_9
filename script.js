"use strict"

//перше завдання
let firstSearchFeature = document.getElementsByClassName("feature");
console.log(firstSearchFeature);

let secondSearchFeature = document.querySelectorAll(".feature");
console.log(secondSearchFeature);

secondSearchFeature.forEach(element => {
    element.style.textAlign = "center"
})

//завдання друге

const title = document.querySelectorAll("h2");

title.forEach(element => {
    element.textContent = "Awesome feature"
})

//завдання третє
const featureTitle = document.querySelectorAll(".feature-title");

featureTitle.forEach(element => {
    element.textContent += "!"
})
